FROM node:14-alpine
RUN rm -f /etc/apk/repositories \
    && echo "http://nl.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories \
    && echo "http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && echo "http://nl.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && apk update \
    && apk upgrade --force

RUN apk --update add git git-lfs python3 docker docker-compose vips-dev fftw-dev libjpeg-turbo-dev jpeg-dev
